<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/dashboard', function () {
    return redirect()->route('websites.my-websites');
})->middleware(['auth'])->name('dashboard');

Route::namespace('App\Http\Controllers')
    ->middleware(['auth'])->group(function() {
    Route::resource('websites', 'WebsiteController');
    Route::get('/', 'WebsiteController@index');
    Route::get('my-websites', 'WebsiteController@index')->name('websites.my-websites');
});


require __DIR__.'/auth.php';

Auth::routes();
