
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="col-md-12">
        <div class="row">
            @foreach ($websites as $website)
                <div class="col-md-4">
                    <div class="card mt-3">
                        <div class="card-header">
                            {{ $website->name }}
                            <div class="float-right">
                                <a href="{{ route('websites.edit', $website->id) }}" class="btn btn-sm btn-secondary">Edit</a>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row p-3">
                                @if(strpos($website->link, 'https://') !== false || strpos($website->link, 'http://') !== false)
                                    <a href="{{ $website->link }}" target="_blank">{{ $website->link }}</a>
                                @else
                                    <a href="{{ 'https://' . $website->link }}" target="_blank">{{ $website->link }}</a>
                                @endif
                            </div>
                            @if (isset($website->description))
                                <div class="row p-3 website-description">{{ $website->description }}</div>
                            @else
                                <div class="row p-3 website-description"><i>No description provided.</i></div>
                            @endif
                            <div class="row p-3 website-status" data-id="{{ $website->id }}" data-link="{{ $website->link }}">
                                <span class="badge badge-pill badge-info website-status-pill">Connecting...</span>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script type="text/javascript">
        function checkWebsites(){
            $.each($('.website-status'), function(index, val){
                let websiteStatusItem = $('.website-status')[index];
                $.get(baseUrl + "/api/get-api-data",{
                    url: $(websiteStatusItem).data('link'),
                }).done(function(){
                    $(websiteStatusItem).html('<span class="badge badge-pill badge-success website-status-pill">Active</span>');
                }).fail(function(result, status){
                    $(websiteStatusItem).html('<span class="badge badge-pill badge-danger website-status-pill">'+status+'</span>');
                });
            });
        }

        checkWebsites();

        setInterval(function(){
            checkWebsites();
        }, 10000);
    </script>
@endsection
