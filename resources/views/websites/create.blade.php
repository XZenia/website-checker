@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Add a new Website</div>
                <div class="card-body">
                    <form action="{{ route('websites.store') }}" method="POST">
                        @csrf
                        <div class="">
                            <label class="block text-sm text-gray-00" for="name">Name</label>
                            <input class="form-control rounded" id="name" name="name" type="text" required="" placeholder="Website Name" aria-label="Name">
                        </div>
                        <div class="mt-5">
                            <label class="block text-sm text-gray-600" for="cus_email">Description</label>
                            <textarea class="form-control rounded" name="description"></textarea>
                        </div>
                        <div class="mt-5">
                            <label class="block text-sm text-gray-00" for="link">Website Link</label>
                            <input class="form-control  rounded" id="link" name="link" type="text" required="" placeholder="Website Link" aria-label="Link">
                        </div>
                        <div class="mt-5">
                            <button type="submit" class="btn btn-primary float-right">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


