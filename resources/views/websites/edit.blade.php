@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit {{ $website->name }}</div>
                <div class="card-body">
                    <form action="{{ route('websites.update', $website->id) }}" method="POST">
                        @csrf
                        @method("PUT")
                        <div class="">
                            <label class="block text-sm text-gray-00" for="name">Name</label>
                            <input class="form-control rounded" id="name" name="name" type="text" required="" value="{{ $website->name }}" placeholder="Website Name" aria-label="Name">
                        </div>
                        <div class="mt-5">
                            <label class="block text-sm text-gray-600" for="cus_email">Description</label>
                            <textarea class="form-control rounded" name="description">{{ $website->description }}</textarea>
                        </div>
                        <div class="mt-5">
                            <label class="block text-sm text-gray-00" for="link">Website Link</label>
                            <input class="form-control  rounded" id="link" name="link" type="text" required="" value="{{ $website->link }}" placeholder="Website Link" aria-label="Link">
                        </div>
                        <div class="mt-5">
                            <button type="submit" class="btn btn-primary float-right">Submit</button>
                            <form action="{{ route('websites.destroy', $website->id) }}" method="POST">
                                @csrf
                                @method("DELETE")
                                <button type="submit" class="btn btn-danger float-right mr-3">Delete</button>
                            </form>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


